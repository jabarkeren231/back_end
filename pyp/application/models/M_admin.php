<?php

class M_admin extends CI_Model{


public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function read(){
		return $this->db->get('admin');

	}
        function input_data($data,$table){
        $this->db->insert($table,$data);
    } 
    function edit_data($where,$table){      
        return $this->db->get_where($table,$where);
    }
 
    function update_data($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }
}
?>